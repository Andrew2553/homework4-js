// Функции - это инструмент, который позволяет группировать повторяющиеся действия внутри отдельных блоков кода. Они выполняют определенные задачи или операции и могут быть вызваны из разных мест программы.

// Функции принимают аргументы, потому что это позволяет нам передавать данные или значения в функцию для последующего использования в ее внутреннем коде.

// Оператор return используется в программировании для возврата значения из функции. Он указывает, что функция завершает свое выполнение и возвращает определенное значение обратно в место вызова функции.


function performOperation(num1, num2, operator) {
    let result;
  
    switch (operator) {
      case '+':
        result = num1 + num2;
        break;
      case '-':
        result = num1 - num2;
        break;
      case '*':
        result = num1 * num2;
        break;
      case '/':
        result = num1 / num2;
        break;
      default:
        console.log('Невірна операція');
        return;
    }
  
    console.log(`Результат: ${num1} ${operator} ${num2} = ${result}`);
  }
  
  function enterNumericValue(promptMessage) {
    let value;
    do {
      value = +prompt(promptMessage);
    } while (isNaN(value));
  
    return value;
  }
  
  let number1 = enterNumericValue('Введіть перше число:');
  let number2 = enterNumericValue('Введіть друге число:');
  let operation;
  
  do {
    operation = prompt('Введіть математичну операцію (+, -, *, /):');
  } while (!['+', '-', '*', '/'].includes(operation));
  
  performOperation(number1, number2, operation);
  